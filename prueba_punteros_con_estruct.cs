
using System;  
struct prueba_punteros_con_estruct  
{  
    public int x;  
    public int y;  

    //Jhustin Ismael Arias Perez 4-A T/M

    public void declararXY(int i, int j)  
    {  
        x = i;  
        y = j;  
    }  
    public void mostrarXY()  
    {  
        Console.WriteLine(x);  
        Console.WriteLine(y);  
    }  
}  
class cliente  
{  
    public unsafe static void Main()  
    {  
        cliente ms = new cliente();  
        cliente* ms1 = &ms;  
        ms1->declararXY(10, 20);  
        ms1->mostrarXY();  
    }  
}