
using System;  
class prueba_unsafe  
{   
    //Jhustin Ismael Arias Perez 4-A T/M

unsafe static void BlueFilter (int[,] bitmap)
{
  int length = bitmap.Length;
  fixed (int* b = bitmap)
  {
	int* p = b;
	for (int i = 0; i < length; i++)
	  *p++ &= 0xFF;
  }
}

static void Main()
{
	int[,] bitmap = { {0x101010, 0x808080, 0xFFFFFF}, {0x101010, 0x808080, 0xFFFFFF} };
	BlueFilter (bitmap);
	bitmap.Dump();
}
}