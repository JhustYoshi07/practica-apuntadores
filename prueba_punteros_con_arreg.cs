
using System;  
class prueba_punteros_con_arreg  
{  
	
    //Jhustin Ismael Arias Perez 4-A T/M

    public unsafe void metodo()  
    {  
        int[] iArray = new int[10];  
        for (int count = 0; count < 10; count++)  
        {  
            iArray[count] = count * count;  
        }  
        fixed (int* ptr = iArray)  
            Display(ptr);  
        //Console.WriteLine(*(ptr+2));  
        //Console.WriteLine((int)ptr);   
    }  
    public unsafe void Display(int* pt)  
    {  
        for (int i = 0; i < 14; i++)  
        {  
            Console.WriteLine(*(pt + i));  
        }  
    }  
}  
class cliente  
{  
    public static void Main()  
    {  
        cliente mc = new cliente();  
        mc.metodo();  
    }  
}  